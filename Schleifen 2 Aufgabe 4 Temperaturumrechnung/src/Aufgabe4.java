import java.util.Scanner;

public class Aufgabe4 {
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Geben Sie den Startwert (Grad Celsius) ein!");
    double startwert = myScanner.nextDouble();
    System.out.println("Geben Sie den Endwert (Grad Celsius) ein!");
    double endwert = myScanner.nextDouble();
    System.out.println("Geben Sie die Schrittweite ein!");
    double schrittweite = myScanner.nextDouble();

    double cwert, fwert;
    cwert = startwert;
    
    	while(cwert < endwert) {
    		fwert = cwert * 1.8 + 32;
    		System.out.println(cwert + " Grad Celsius      " + fwert + "  Fahrenheit");
    		cwert = cwert + schrittweite;
    	}
  }
}