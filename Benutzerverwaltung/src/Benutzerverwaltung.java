import java.util.ArrayList;
import java.util.Scanner;


public class Benutzerverwaltung {

	public static void main(String[] args) {
		
		ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();
		int auswahl;

		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				// benutzerAnzeigen();
					System.out.println(benutzerliste);
				break;
			case 2:
				 benutzerErfassen(benutzerliste);
				
				break;
			case 3:
				// benutzerLöschen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");				
			}
		}while (true );
		
	}
	
	public static int menu() {

        int selection;
        Scanner input = new Scanner(System.in);

        /***************************************************/

        System.out.println("     Benutzerverwaltung     ");
        System.out.println("----------------------------\n");
        System.out.println("1 - Benutzer anzeigen");
        System.out.println("2 - Benutzer erfassen");
        System.out.println("3 - Benutzer löschen");
        System.out.println("4 - Ende");
         
        System.out.print("\nEingabe:  ");
        selection = input.nextInt();
        return selection;    
    }
	
	public static void benutzerErfassen(ArrayList<Benutzer> benutzerliste) {
		
		int bnr;
		String vorname;
		String nachname;
		int gjahr;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie die Benutzernummer ein!");
		bnr = input.nextInt();
		System.out.println("Bitte geben Sie den Vornamen ein!");
		vorname = input.next();
		System.out.println("Bitte geben Sie den Nachnamen ein!");
		nachname = input.next();
		System.out.println("Bitte geben Sie das Geburtsjahr ein!");
		gjahr = input.nextInt();
		
		Benutzer b1 = new Benutzer (bnr, vorname, nachname, gjahr);
		benutzerliste.add(b1);
	}

}