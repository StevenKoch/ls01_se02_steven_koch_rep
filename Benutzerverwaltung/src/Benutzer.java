import java.text.SimpleDateFormat;
import java.util.Date;

public class Benutzer {

		private int benutzernr;
		private String vname;
		private String nname;
		private int geburtsjahr;
		
		public Benutzer() {
			
		}
		
		public Benutzer(int benutzernr, String vname, String nname, int geburtsjahr) {
			
			this.benutzernr = benutzernr;
			this.vname = vname;
			this.nname = nname;
			this.geburtsjahr = geburtsjahr;
		}
		
		public void setBenutzernr(int benutzernr) {
			this.benutzernr = benutzernr;
		}
		
		public int getBenutzernr() {
			return benutzernr;
		}
		
		public void setVname(String vname) {
			this.vname = vname;
		}
		
		public String getVname() {
			return vname;
		}	
		
		public void setNname(String nname) {
			this.nname = nname;
		}
		
		public String getNname() {
			return nname;
		}
		
		public void setGeburtsjahr(int geburtsjahr) {
			this.geburtsjahr = geburtsjahr;
		}
		
		public int getGeburtsjahr() {
			return geburtsjahr;
		}
		
		public int getAlter() {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
			Date date = new Date();
			int aYear = Integer.parseInt(formatter.format(date));
			int alter = aYear - geburtsjahr;
			return alter;
		}
		
		@Override
		public String toString() {
		    return "Benutzernummer: " + this.benutzernr + " | Vorname: " + this.vname + " | Nachname: " + this.nname
		            + " | Geburtsjahr " + this.geburtsjahr + " ";
		}
		
		

}
