import java.util.Scanner;

public class Aufgabe3 {
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    int summe = 0;
    
    System.out.print("Geben sie bitte eine Zahl ein: ");
    int zahl = myScanner.nextInt();
    
    while (0 != zahl) {
      summe += zahl % 10;
      zahl /= 10;					// zahl = zahl / 10
    }
    
    System.out.println("Die Quersumme ist: " + summe);
  }
}