﻿/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  int Durchlauf ;			 
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	      Durchlauf = 25;
	    System.out.println("Durchläufe: " + Durchlauf );
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
     String  Menue ;
    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
      	   Menue = "C";
      	System.out.println("\nMenüpunkt: " + Menue);
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
     long AstroZahl;
    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
     AstroZahl = 299792;
     	System.out.println("\nDie Lichtgeschwindigkeit beträgt: " + AstroZahl + " km/s");
    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
   	 int Mitglieder;
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
   	 Mitglieder = 7;
   	 	System.out.println("\nAnzahl der Mitglieder des Vereins: " + Mitglieder);
    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
   	double Ladung = 1.602E-19;
   		System.out.println("\nDie elektrische Elementarladung beträgt:  " + Ladung);
    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
   	String Buchung;

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
   	Buchung = "ist erfolgt";
   		System.out.println("\nBuchung " + Buchung);

  }//main
}// Variablen