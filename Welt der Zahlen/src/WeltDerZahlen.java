/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstraße
    long    anzahlSterne = 200000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int    bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short   alterTage = 9689;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int   gewichtKilogramm =   190000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int   flaecheGroessteLand = 17098242;
    
    // Wie groß ist das kleinste Land der Erde?
    
    float   flaecheKleinsteLand = 0.44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Bewohner in Berlin: " + bewohnerBerlin);
    
    System.out.println("Mein Alter in Tagen " + alterTage);
    
    System.out.println("Das Gewicht des Blauwales betraegt: " + gewichtKilogramm + "kg");
    
    System.out.println("Die Fl�che Russlands betraegt: " + flaecheGroessteLand + "km�");
    
    System.out.println("Die Fl�che des Vatikans betraegt: " + flaecheKleinsteLand + "km�");
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

