import java.util.Scanner;

class Fahrkartenautomat06
{
    public static void main(String[] args)
    {
       
    	int wartungsZeitraum = 0;
    	while(wartungsZeitraum < 999) {
    	rueckgeldAusgeben();
    	wartungsZeitraum += 1;
    	}
    	System.out.println("Der Automat hat das Ende seines Wartungszeitraumes erreicht. Bitte kontaktieren Sie den Kundenservice!");
    	//Wir h�tten hier auch while(true) verwenden  k�nnen, aber so ein Wartungszeitraum f�gt noch ein bisschen extra Realismus-Pepp hinzu :D

    }
    
    //Bestellaufnahme und R�ckgabe zuZahlen
    public static double fahrkartenbestellungErfassen(){
    	Scanner tastatur = new Scanner(System.in);
    	System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
    	System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
    	System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
    	System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    	
    	double preisTicket = 0;
    	boolean standardAusgabe = false; //sorgt daf�r, dass bei ung�ltiger Eingabe die Abfrage wiederholt wird
    	
    	while(standardAusgabe == false) {
    		System.out.println("Ihre Wahl?: ");
    		int fahrkartenArt = tastatur.nextInt();
    	switch(fahrkartenArt) {
    	case 1:
    		preisTicket = ( 2.90 * 100); //KONVERTIERUNG TICKETPREIS IN CENT
    		standardAusgabe = true;
    		break;
    	case 2:
    		preisTicket = ( 8.60 * 100); //KONVERTIERUNG TICKETPREIS IN CENT
    		standardAusgabe = true;
    		break;
    	case 3:
    		preisTicket = ( 23.50 * 100); //KONVERTIERUNG TICKETPREIS IN CENT
    		standardAusgabe = true;
    		break;
    	default:
    		System.out.println("Ung�ltige Eingabe. Bitte w�hlen Sie eine g�ltige Eingabe!");
    		break;
    		}
    	}
    	//Switch f�r Ticketauswahl zust�ndig
    	
    	System.out.println("Wie viele Tickets wollen Sie kaufen?: ");
    	int anzahlTicket = tastatur.nextInt();
    	double zuZahlen = anzahlTicket * preisTicket;
    	return zuZahlen;
    }
    
    public static double fahrkartenBezahlen() {
    	double zuZahlen = fahrkartenbestellungErfassen();
    	//M�nzeinwurf
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("%s %.2f %s %n" , "Noch zu zahlen: ", (zuZahlen - eingezahlterGesamtbetrag) / 100, "Euro");	//KONVERTIERUNG D. AUSGABE IN EURO
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   double eingeworfeneM�nze = (tastatur.nextDouble() * 100);	//KONVERTIEREN EINGEWORFENE M�NZEN IN CENT
            eingezahlterGesamtbetrag += eingeworfeneM�nze;                  
        }
        //Verrechnung zuZahlen & eingezahlter Gesamtbetrag
        double rueckgabeBetrag = (eingezahlterGesamtbetrag - zuZahlen) / 100;	//KONVERTIERUNG rueckgabeBetrag IN EURO
        return rueckgabeBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	//Fahrscheinausgabe Benachrichtigung
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 50; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(100);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");	
    }
    
    public static void rueckgeldAusgeben() {
    	double rueckgabeBetrag = fahrkartenBezahlen();	//Aufruf der fahrkartenBezahlen Methode da Wert rueckgabeBetrag ben�tigt wird
    	fahrkartenAusgeben();
    	if(rueckgabeBetrag > 0.0)	
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", (rueckgabeBetrag));
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgabeBetrag >= 2.00) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          rueckgabeBetrag -= 2.00;
            }
            while(rueckgabeBetrag >= 1.00) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          rueckgabeBetrag -= 1.00;
            }
            while(rueckgabeBetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          rueckgabeBetrag -= 0.50;
            }
            while(rueckgabeBetrag >= 0.20) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          rueckgabeBetrag -= 0.20;
            }
            while(rueckgabeBetrag >= 0.10) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          rueckgabeBetrag -= 0.10;
            }
            while(rueckgabeBetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          rueckgabeBetrag -= 0.05;
            }
        }    	
    }
    
    
    
}