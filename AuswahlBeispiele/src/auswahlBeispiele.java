
public class auswahlBeispiele {
	
	public static void main(String[] args) {
		
		
		
		double erg = max2(2.5,7.2,3.6);
		System.out.println("Ergebnis : " + erg);
		
	}

	public static int min(int zahl1, int zahl2) {
		int erg;
		if (zahl1 < zahl2) {
			erg = zahl1;
		}
		else {
			erg = zahl2;
		}
		return erg;
	}
	
	public static int max(int zahl1, int zahl2) {
		int erg;
		if (zahl1 > zahl2) {
			erg = zahl1;
		}
		else {
			erg = zahl2;
		}
		return erg;
	}
	
	public static double max2(double zahl1, double zahl2, double zahl3) {
		double erg;
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			erg = zahl1;
		}
		
		if (zahl2 > zahl1 && zahl2 > zahl3) {
			erg = zahl2;
		}
		
		else {
			erg = zahl3;
		}
		return erg;
	}
}
