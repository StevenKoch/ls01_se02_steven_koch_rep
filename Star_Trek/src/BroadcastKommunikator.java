import java.util.ArrayList;

public class BroadcastKommunikator {
    private ArrayList<String> bck;

    public BroadcastKommunikator() {
        this.bck = new ArrayList<String>();
    }

    public void addMessage(String message) {
        bck.add(message);
    }

    public ArrayList<String> getMessages() {
        return bck;
    }
}
