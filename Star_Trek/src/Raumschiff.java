import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

    private String schiffName; // Schiffs Name
    private int anzahlTP; // Anzahl Torpedos
    private int eiP; // Enerigeversorgung in Prozent
    private int siP; // schilde in Prozent
    private int hiP; // Hülle in Prozent
    private int liP; // Lebenserhaltung in Prozent
    private int anA; // Anzahl Androiden
    private ArrayList<String> bcK; // Broadcast Kommunikator
    private ArrayList<Ladung> lAV; // Ladungsverzeichnis

    // null Konstruktor
    public Raumschiff() {
        this.bcK = new ArrayList<String>();
        this.lAV = new ArrayList<Ladung>();
    }

    // Parameterisierter Konstruktor
    public Raumschiff(String schiffName, int anzahlTP, int eiP, int sip, int hip, int lip, int anA) {
        this.schiffName = schiffName;
        this.anzahlTP = anzahlTP;
        this.eiP = eiP;
        this.siP = sip;
        this.hiP = hip;
        this.liP = lip;
        this.anA = anA;
        this.bcK = new ArrayList<String>();
        this.lAV = new ArrayList<Ladung>();
    }

    // get/set SchiffName
    public void setSchiffName(String name) {
        this.schiffName = name;
    }

    public String getSchiffName() {
        return schiffName;
    }

    // get/set Anzahl Torpedos
    public void setAnzahlTP(int anzahlTP) {
        this.anzahlTP = anzahlTP;
    }

    public int getAnzahlTP() {
        return anzahlTP;
    }

    // get/set Energieversorgung in Prozent
    public void setEiP(int eiP) {
        this.eiP = eiP;
    }

    public int getEiP() {
        return eiP;
    }

    // get/set Schilde in Prozent
    public void setSiP(int siP) {
        this.siP = siP;
    }

    public int getSiP() {
        return siP;
    }

    // get/set Hülle in Prozent
    public void setHiP(int hiP) {
        this.hiP = hiP;
    }

    public int getHiP() {
        return hiP;
    }

    // get/set Lebenserhaltungssysteme in Prozent
    public void setLip(int liP) {
        this.liP = liP;
    }

    public int getLip() {
        return liP;
    }

    // get/set Anzahl Androiden
    public void setAnA(int anA) {
        this.anA = anA;
    }

    public int getAnA() {
        return anA;
    }

    //////////////////////////////

    public void useAndroids(Boolean shipEnergy, Boolean shipHull, Boolean shipShield, Boolean shipLife,
            int counterAndroids) {

        Random randomizer = new Random();
        int randomNumber = randomizer.nextInt(101);

        if (counterAndroids >= anA) {
            counterAndroids = anA;
        }

        int countShipDepartments = 0;
        if (shipEnergy == true) {
            countShipDepartments++;
        }
        if (shipHull == true) {
            countShipDepartments++;
        }
        if (shipShield == true) {
            countShipDepartments++;
        }
        if (shipLife == true) {
            countShipDepartments++;
        }
        int percentCalculation = ((randomNumber * counterAndroids) / (countShipDepartments));

        if (shipEnergy == true) {
            setEiP(eiP += percentCalculation);
        }
        if (shipHull == true) {
            setHiP(hiP += percentCalculation);
        }
        if (shipShield == true) {
            setSiP(siP += percentCalculation);
        }
        if (shipLife == true) {
            setLip(liP += percentCalculation);
        }

    }

    /**
     * Hiermit fügen wir eine Ladung dem Ladungsverzeichnis hinzu
     * 
     * @param newLoad
     */
    public void addLoad(Ladung newLoad) {
        this.lAV.add(newLoad);
        System.out.println(newLoad.getBezeichnung() + " got the Index Number: " + (lAV.size() - 1));
    }

    /**
     * Hiermit verbauchen wir eine Ladung
     * 
     * @param index
     */
    public void useLoad(int index) {
        lAV.get(index).setMenge((lAV.get(index).getMenge() - 1));

        if (lAV.get(index).getMenge() == 0) {
            lAV.remove(index);
            System.out.println("[INFO]Removed Ladung with number: " + index + " because it is empty");
        }
    }

    /**
     * Hiermit schießen wir die Torpedos ab solange welche Vorhanden sind.
     * 
     * @param eR
     * @category Angriff
     */
    public void tPSchuss(Raumschiff eR, BroadcastKommunikator bcKExternal) { // Torpedo Schießen
        if (anzahlTP == 0) {
            System.out.println("[INFO] Keine Torpedos Vorhanden!");
            bcK.add("-=*Klick*=-");
            bcKExternal.addMessage("-=*Klick*=-");
        } else {
            anzahlTP -= 1;
            System.out.println("[INFO] Torpedo Abgeschossen");
            bcK.add("Photonentorpedo abgeschossen!");
            bcKExternal.addMessage("Photonentorpedo abgeschossen!");
        }
    }

    /**
     * Hiermit Schießen wir den Phaser ab solange die Erergie des schiffs bei über
     * 50% liegt
     * 
     * @param r1
     * @category Angriff
     * 
     */
    public void phSchuss(Raumschiff r1, BroadcastKommunikator bcKExternal) {
        if (eiP < 50) {
            System.out.println("[INFO] Phaser können nicht abgeschossen werden => Energie zu gering");
            bcK.add("-=*Click*=-");
            bcKExternal.addMessage("-=*Klick*=-");

        } else {
            eiP -= 50;
            bcK.add("Phaser wurde abgeschossen!");
            bcKExternal.addMessage("Phaser wurde abgeschossen!");
            treffer(r1, bcKExternal);
        }
    }

    private void treffer(Raumschiff r1, BroadcastKommunikator bckExternal) {
        trefferVermerken(r1, bckExternal);
    }

    /**
     * Hiermit vermerken wir in der Konsole, dass ein Schiff getroffen wurde.
     * 
     * @param r1
     * 
     * @category Angriff
     */
    private void trefferVermerken(Raumschiff r1, BroadcastKommunikator bckExternal) {
        System.out.println("[INFO] '" + r1.getSchiffName() + "' wurde getroffen!");

        r1.setSiP((r1.getSiP() / 50));
        if (r1.getSiP() <= 0) {
            r1.setHiP((r1.getHiP() / 2));
            r1.setLip((r1.getLip() / 50));
            if (r1.getHiP() <= 0) {
                r1.setLip(0);
                bcK.add("[INFO] Lebenserhaltungssysteme des Feindlichen Schiffs '" + r1.getSchiffName() + "' Zerstört");
                bckExternal
                        .addMessage("[INFO] Lebenserhaltungssysteme des Schiffs '" + r1.getSchiffName() + "' Zerstört");
            }
        }
    }

    /**
     * Hiermit nutzen wir die Photonentorpedos.
     * 
     * @param bckExternal
     */
    public void useTP(BroadcastKommunikator bckExternal, int userWantUse) {
        if (anzahlTP == 0) {
            System.out.println("[INFO] Keine Photonentorpedos gefunden!");
            bckExternal.addMessage("-=*Click*=-");
        } else {
            if (userWantUse <= anzahlTP) {
                System.out.println("[" + userWantUse + "] Photonentorpedo(s) eingesetzt.");
                this.anzahlTP -= userWantUse;
            } else {
                while (userWantUse > anzahlTP) {
                    userWantUse -= 1;
                }
                if (userWantUse == 0) {
                    System.out.println("[INFO] Keine Photonentorpedos gefunden!");
                    bckExternal.addMessage("-=*Click*=-");
                } else {
                    System.out.println("[" + userWantUse + "] Photonentorpedo(s) eingesetzt.");
                    this.anzahlTP -= userWantUse;
                }
            }
        }
    }

    /**
     * Gibt den Kompletten Zustand des Raumschiffes in die Console aus.
     * 
     * @category Information
     */
    public void zustand() {
        System.out.println("[" + schiffName + "]");
        System.out.println("        Anzahl Torpedos:                " + anzahlTP);
        System.out.println("        Energieversorgung in Prozent:   " + eiP);
        System.out.println("        Schilde in Prozent:             " + siP);
        System.out.println("        Huelle in Prozent:              " + hiP);
        System.out.println("        Lebenserhaltung in Prozent:     " + liP);
        System.out.println("        Anzahl Androiden:               " + anA);
        System.out.println("        Kommunikator Log Groesse:       " + bcK.size());
        System.out.println("        Ladungsverzeichnis Groesse:     " + lAV.size());
    }

    /**
     * Diese Methode wird benutzt um eine Message im Broadcast Kommunikator des
     * Schiffes zu Hinterlegen
     * 
     * @param message
     */
    public void addBkMessage(String message) {
        bcK.add(message);
    }

    public ArrayList<String> getBCKMessages() {
        return bcK;
    }

    public void getBCKMessagesString() {
        System.out.println(bcK);
    }

    /**
     * Prints out the Loading Documents
     */
    public void getLAV() {
        System.out.println(lAV);
    }
}
